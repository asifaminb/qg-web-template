import './qg-social-media';
import './progressive-reveal';
import './license';
import './carousel/carousel';
import './quick-exit/quick-exit';
import './accordion/accordion';
import './forms/recaptcha';
import './forms/qg-address-autocomplete';
import './qg-document-links';
import './gallery/qg-gallery';
import accessibility      from './accessibility/accessibility';

accessibility.init();
