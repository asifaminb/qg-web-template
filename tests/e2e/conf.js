/*eslint-disable */
exports.config = {
  onPrepare: function () {
    const protractorImageComparison = require('protractor-image-comparison');
    browser.protractorImageComparison = new protractorImageComparison(
      {
        baselineFolder: '/screenshots',
        screenshotPath: './tests/e2e/screenshots',
      }
    );
  },
  directConnect: true,
  capabilities: {
    browserName: 'chrome',

    chromeOptions: {
      args: ["--headless", '--disable-gpu', '--window-size=2000,1000' ],
    },
  },
  baseUrl: 'http://localhost:8086/',
  specs: ['spec/*.spec.js'],
};
